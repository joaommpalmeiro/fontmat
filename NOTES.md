# Notes

- https://github.com/joaopalmeiro/template-python-cli
- fontTools:
  - https://github.com/fonttools/fonttools
  - https://fonttools.readthedocs.io/en/latest/
  - https://pypi.org/project/fonttools/
  - https://github.com/fonttools/fonttools/releases
  - https://fonttools.readthedocs.io/en/latest/optional.html
  - https://fonttools.readthedocs.io/en/latest/ttLib/index.html
  - https://fonttools.readthedocs.io/en/latest/ttLib/ttFont.html#fontTools.ttLib.ttFont.TTFont
  - https://github.com/google/brotli
  - Naming Table:
    - https://fonttools.readthedocs.io/en/latest/ttLib/tables/_n_a_m_e.html
      - https://fonttools.readthedocs.io/en/latest/ttLib/tables/_n_a_m_e.html#fontTools.ttLib.tables._n_a_m_e.table__n_a_m_e
      - https://fonttools.readthedocs.io/en/latest/_modules/fontTools/ttLib/tables/_n_a_m_e.html#table__n_a_m_e.getBestFullName
    - https://learn.microsoft.com/en-us/typography/opentype/spec/name
      - https://learn.microsoft.com/en-us/typography/opentype/spec/name#name-ids
    - https://github.com/fonttools/fonttools/blob/4.53.0/Lib/fontTools/ttLib/tables/_n_a_m_e.py#L147
    - https://gist.github.com/pklaus/dce37521579513c574d0
- Fontsource:
  - https://fontsource.org/fonts/roboto
  - https://github.com/fontsource/font-files/tree/main/fonts/google/roboto
  - https://github.com/google/fonts/tree/main?tab=readme-ov-file#self-host-fonts-available-from-google-fonts: "One popular service is Fontsource, which offers bundled NPM packages."
  - https://github.com/fontsource/font-files/blob/025ea945ff916c7bfe4fe5aaef1f3f57a523a1c6/fonts/google/roboto/metadata.json
  - https://www.npmjs.com/package/@fontsource/roboto
- https://docs.safetycli.com/safety-docs/pyup.io-is-now-safety-cybersecurity
- https://fontdrop.info/
- https://fonts.google.com/knowledge/using_type/using_web_fonts:
  - "(...) `font-display`. By providing a value of `swap`, we tell the browser to render the page right away with fallback fonts, and then redraw the page once the fonts have loaded."
- https://github.com/vercel/next.js/tree/canary/packages/font
- https://github.com/googlefonts/roboto/issues/287
- https://fonts.googleapis.com/css2?family=Roboto
- https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap
- https://en.wikipedia.org/wiki/Latin_Extended-A
- https://en.wikipedia.org/wiki/Latin_script_in_Unicode
- https://github.com/google/fonts/blob/4d015b57411aa9dfddb89655670b3f2a2834419e/ofl/roboto/METADATA.pb
- https://github.com/googlefonts/googlefonts.github.io/blob/6f94e0551784b73e5d966469b1d88e9af783839e/gf-guide/build.md#L151
- https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face#specifying_local_font_alternatives:
  - "In this example, the user's local copy of "Helvetica Neue Bold" is used; if the user does not have that font installed (both the full font name and the Postscript name are tried) (...)"
- https://docs.python.org/3.10/library/typing.html#typing.TypedDict
- https://docs.python.org/3.10/library/typing.html#typing.Tuple: "To specify a variable-length tuple of homogeneous type, use literal ellipsis, e.g. `Tuple[int, ...]`."

## Commands

```bash
hatch env prune && hatch env create
```

## Snippets

```css
@font-face {
  font-family: "FAMILY_NAME";
  font-style: NORMAL_OR_ITALIC;
  font-weight: NUMERIC_WEIGHT_VALUE;
  font-display: swap;
  src: url(FONT_FILE_NAME.woff2) format("woff2");
}
```

### Check all name records (Naming Table)

```python
from fontTools import ttLib

with ttLib.TTFont(filename) as f:
    print(f["name"].names)
    print(
        [f["name"].getDebugName(name_record.nameID) for name_record in f["name"].names]
    )
```
